#!/usr/bin/python3
#Copyright (c) 2015 Mary Infinity
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

from bs4 import BeautifulSoup
from PyPDF2 import PdfFileMerger
from wand.image import Image
import os
import sys
import tempfile
import urllib.request

website = urllib.request.urlopen(sys.argv[1])
initialpage = BeautifulSoup(website, 'html.parser')

# Get URL of jpg from the HTML
for metaobject in initialpage.find_all('link'):
    if metaobject.get('rel') == ['image_src']:
        pageoneurl = metaobject.get('href')

# Get the documentID from the URL of the jpg
temptable = pageoneurl.split('issuu.com/')
temptable = temptable[1].split('/jpg')
documentId = temptable[0]
#Download all jpeg files, saving them to a filelist. We'll know that we've
#finished when we get a HTTPError
count = 0
filelist = []
while count != 'finished':
    count = count + 1
    print('Downloading page '+str(count))
    jpegurl='http://image.issuu.com/'+documentId+'/jpg/page_'+str(count)+'.jpg'
    try:
        downloadedjpeg = urllib.request.urlopen(jpegurl)
        jpeglocation=tempfile.NamedTemporaryFile(prefix='issuu-'+str(count)+'-',suffix='.jpg',delete=False)
        jpeglocation.write(downloadedjpeg.read())
        jpeglocation.close()
        filelist.append(jpeglocation.name)
    except urllib.error.HTTPError:
        # A 403 error means that there are no more pages left
        print('All of the files have been downloaded. Please wait while the PDF is generated.')
        count = 'finished'

#Convert all of the JPEG files to PDF, then combine the PDF files
count2 = 0
filelist2 = []
merge = PdfFileMerger()
for picture in filelist:
    count2 = count2 + 1
    with Image(filename=picture) as img:
        img.format = 'pdf'
        pdflocation=tempfile.NamedTemporaryFile(prefix='issuu-pdf'+str(count2)+'-',suffix='.pdf',delete=False)
        img.save(filename=pdflocation.name)
        pdflocation.close()
        filelist2.append(pdflocation.name)
        openedpdf = open(pdflocation.name,'rb')
        merge.append(openedpdf)

print('Deleting temporary files')
for file in filelist:
    os.remove(file)
for file in filelist2:
    os.remove(file)

output = open('issuu-'+documentId+'.pdf','wb')
merge.write(output)
